package test;

import java.io.File;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Test1 {
    private WebDriver driver;

    @BeforeClass
    public void beforeClass() {
        System.setProperty("webdriver.chrome.driver","driver"+ File.separator +"chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(5, TimeUnit.SECONDS);
        driver.get("https://www.falabella.com/falabella-cl");
    }

    @AfterClass
    public void afterClass() throws InterruptedException {
        Thread.sleep(5000);
        driver.quit();
    }

    @Test
    public void verifySearchButton() throws InterruptedException {
        //desplegar categorias
        WebElement btnMenu = driver.findElement(By.id("hamburgerMenu"));
        Test1.waitVisibility(driver,btnMenu,10);
        btnMenu.click();
        //seleccionar tecnologia
        WebElement tecnologiaOpt = driver.findElement(By.xpath("(//*[@class='FirstLevelItems_menuText__UYB9A'])[2]"));
        Test1.waitVisibility(driver,tecnologiaOpt,10);
        tecnologiaOpt.click();
        //Seleccionar consola
        WebElement consolaTxt = driver.findElement(By.xpath("//*[contains(text(), 'Consolas')]"));
        //  Test1.waitVisibility(driver,consolaTxt,10);
        consolaTxt.click();
        Thread.sleep(2000);
        //Seleccionar filtro de marca
        WebElement filtroMarca = driver.findElement(By.id("testId-Accordion-Marca"));
       // Test1.waitVisibility(driver,filtroMarca,10);
        filtroMarca.click();
        Thread.sleep(2000);
        //seleccionar marca Activision
        WebElement marcaOpt = driver.findElement(By.xpath("//*[contains(text(), 'activision')]"));
        Test1.waitVisibility(driver,marcaOpt,10);
        marcaOpt.click();
        Thread.sleep(3000);
        //ingresar al detalle del producto
        WebElement verProdcutoBtn = driver.findElement(By.xpath("(//*[@class='jsx-110925730 button button-primary'])[2]"));
        Test1.waitVisibility(driver,verProdcutoBtn,10);
        verProdcutoBtn.click();
        //aumentar cantidad a 3
        WebElement aumentarBtn = driver.findElement(By.xpath("(//*[@class='fb-quantity-input__plus'])"));
        Test1.waitVisibility(driver,aumentarBtn,10);
        aumentarBtn.click();
        aumentarBtn.click();
        Thread.sleep(1000);
        System.out.println(Test1.validaCantidadProductos(driver));
        Assert.assertTrue(Test1.validaCantidadProductos(driver),
                "Hubo problemas al seleccionar la cantidad del producto");
    }

    public static void waitVisibility(WebDriver driver, WebElement element, int time) {
        WebDriverWait wait = new WebDriverWait(driver, time);
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    public static void waitClickable(WebDriver driver, WebElement element, int time) {
        WebDriverWait wait = new WebDriverWait(driver, time);
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    public static Boolean validaCantidadProductos(WebDriver driver) {
        WebElement cantidadInp = driver.findElement(By.name("quantity1"));
        String cantUnidadesProd = cantidadInp.getAttribute("value");
        if (cantUnidadesProd.equals("3")) {
            return true;
        }else {
            return false;
        }
    }
}